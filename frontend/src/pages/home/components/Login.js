import React, {useState} from 'react'
import PropTypes from "prop-types";
import axios from "axios";
import Modal from "../../../Modal/Modal"
import "./Login.css"

function useInputValue(background = '', defaultValue = '') {
    const [value, setValue] = useState(defaultValue)

    return {
        bind: {
            value,
            onChange: event => setValue(event.target.value),
            placeholder: background,
            fontSize: '100px',
            size: '23'
        },
        clear: () => setValue(''),
        value: () => value
    }
}

function Login(props) {
    const inputUsName = useInputValue('Username')
    const inputPwd = useInputValue('Password')
    const [modalLoginActive, setModalLoginActive] = React.useState(false)

    function submitCode(event) {
        event.preventDefault()
        props.setUsername(inputUsName.value())
        props.setName(inputUsName.value())
        props.setPwd(inputPwd.value())
        axios.post('https://whatmemareyou.tech/api/v1/authenticate', {
            username: inputUsName.value(),
            pwd: inputPwd.value()
        })
            .then(function (response) {
                console.log(response);
                props.setToken(response.data.token)
            })
            .catch(function (error) {
                console.log(error);
                props.setToken('-1')
            });

        setModalLoginActive(false);
    }

    return (
        <div>
            <button className='loginButton' onClick={() => {
                setModalLoginActive(true);
            }}>Login
            </button>
            <Modal active={modalLoginActive} setActive={setModalLoginActive}>
                <form className='loginButtonsGroup' onSubmit={submitCode}>
                    <p>Login</p>
                    <input {...inputUsName.bind} />
                    <input {...inputPwd.bind} />
                    <button className='lButton' type='submit'>login</button>
                    <button className='closeButton' id='closeClick' onClick={() => {
                        inputUsName.clear();
                        inputPwd.clear();
                        setModalLoginActive(false)
                    }}>+
                    </button>
                </form>
            </Modal>
        </div>)
}

Login.propTypes = {
    setName: PropTypes.func.isRequired,
    setUsername: PropTypes.func.isRequired,
    setPwd: PropTypes.func.isRequired,
    setToken: PropTypes.func.isRequired
}

export default Login
