import React from 'react'
import PropTypes from 'prop-types'
import './Name.css'

function Name(props) {

    return (
        <input className='name' type='text' placeholder="USERNAME" value={props.name === 'anonim' ? '' : props.name}
               onChange={event => props.setName(event.target.value)}/>
    )
}

Name.propTypes = {
    setName: PropTypes.func.isRequired
}

export default Name;
