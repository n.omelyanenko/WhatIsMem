import React, {useState} from 'react'
import Modal from "../../../Modal/Modal"
import "./Register.css"
import axios from "axios";

function useInputValue(background = '', defaultValue = '') {
    const [value, setValue] = useState(defaultValue)

    return {
        bind: {
            value,
            onChange: event => setValue(event.target.value),
            placeholder: background,
            fontSize: '100px',
            size: '23'
        },
        clear: () => setValue(''),
        value: () => value
    }
}

function Register() {
    const inputUsName = useInputValue('Username')
    const inputPwd1 = useInputValue('Password')
    const inputPwd2 = useInputValue('Confirm password')
    const [error, setError] = React.useState('')
    const [modalRegisterActive, setModalRegisterActive] = React.useState(false)
    const [modalResultActive, setModalResultActive] = React.useState(false)
    const [status, setStatus] = React.useState('')

    function submitCode(event) {
        event.preventDefault()
        if (inputPwd1.value() === inputPwd2.value() && inputPwd1.value() !== '') {
            axios.post('https://whatmemareyou.tech/api/v1/register', {
                username: inputUsName.value(),
                password: inputPwd1.value(),
                confirmPassword: inputPwd2.value()
            })
                .then(function (response) {
                    console.log(response.data.token);
                    setStatus('You have successfully registered as ' + inputUsName.value() + '!')
                })
                .catch(function (error) {
                    console.log(error);
                    setStatus('Register error')
                });
            setModalResultActive(true)
            setModalRegisterActive(false);
        } else {
            setError('Incorrect password repeat')
            inputPwd1.clear()
            inputPwd2.clear()
        }
    }

    return (
        <div>
            <button className='registerButton' onClick={() => {
                setError('')
                setModalRegisterActive(true);
            }}>Register
            </button>
            <Modal active={modalRegisterActive} setActive={setModalRegisterActive}>
                <form className='loginButtonsGroup' onSubmit={submitCode}>
                    <p>Register</p>
                    <p style={{
                        float: 'right',
                        color: 'red',
                        fontSize: '12pt',
                    }}>{error}</p>
                    <input {...inputUsName.bind} />
                    <input {...inputPwd1.bind} />
                    <input {...inputPwd2.bind} />
                    <button className='lButton' type='submit'>register</button>
                    <button className='closeButton' id='closeClick' onClick={() => {
                        inputUsName.clear();
                        inputPwd1.clear();
                        inputPwd2.clear();
                        setModalRegisterActive(false)
                    }}>+
                    </button>
                </form>
            </Modal>
            <Modal active={modalResultActive} setActive={setModalResultActive}>
                <p>{status}</p>
            </Modal>
        </div>)
}

export default Register
