import React from 'react'
import {useNavigate} from "react-router-dom";
import PropTypes from "prop-types";
import "./AccountButton.css"

function AccountButton(props) {
    const navigate = useNavigate()
    return (
        <button className='accountButton' onClick={() => {
            navigate('/user', {replace: false, state: {username: props.username, password: props.password, token: props.token}})
        }}>account
        </button>
    )
}

AccountButton.propTypes = {
    username: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    token: PropTypes.string.isRequired
}

export default AccountButton
