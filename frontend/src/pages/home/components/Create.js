import React, {useState} from 'react'
import PropTypes from 'prop-types'
import {useNavigate} from "react-router-dom";
import Modal from "../../../Modal/Modal"
import './Create.css'

function useInputValue(background = '', defaultValue = '') {
    const [value, setValue] = useState(defaultValue)

    return {
        bind: {
            value,
            onChange: event => setValue(event.target.value),
            placeholder: background,
            fontSize: '100px',
            size: '23'
        },
        clear: () => setValue(''),
        value: () => value
    }
}

function Create(props) {
    const inputPlayerNumber = useInputValue('Number of players')
    const inputRoundTime = useInputValue('Round time')
    const inputRoundCount = useInputValue('Number of rounds')
    const navigate = useNavigate()

    const [modalCreateActive, setModalCreateActive] = React.useState(false)
    const [error, setError] = React.useState('')

    function submitCode(event) {
        event.preventDefault()

        let number = Number.parseInt(inputPlayerNumber.value().trim())
        let time = Number.parseInt(inputRoundTime.value().trim())
        let rounds = Number.parseInt(inputRoundCount.value().trim())
        if (number < 2 || number > 10 || isNaN(number)) {
            setError('Число игроков должно быть от 3 до 10!');
            inputPlayerNumber.clear()
        } else if (time < 5 || time > 60 || isNaN(time)) {
            setError('Время на раунд должно быть от 5 до 60 (секунд)!');
            inputRoundTime.clear()
        } else if (rounds < 1 || rounds > 10 || isNaN(rounds)) {
            setError('Количество раундов должно быть от 1 до 10!');
            inputRoundCount.clear()
        } else {
            navigate('/game', {replace: false, state: {name: props.name, numberOfPlayers: number, numberOfRounds: rounds, roundTimeSec: time, code: -1, token: props.token}})
        }
    }

    return (
        <div>
            <button className='createButton' onClick={() => {setModalCreateActive(true); setError('')}}>CREATE GAME</button>
            <Modal active={modalCreateActive} setActive={setModalCreateActive}>
                <form className='createButtonsGroup' onSubmit={submitCode}>
                    <div>
                        <p>Username: {props.name}</p>
                        <p style={{
                            float: 'right',
                            color: 'red',
                            fontSize: '12pt',
                        }}>{error}</p>
                    </div>
                    <input {...inputPlayerNumber.bind} /><br/>
                    <input {...inputRoundTime.bind} /><br/>
                    <input {...inputRoundCount.bind} /><br/>
                    <button className='cButton' type='submit'>create</button>
                    <button className='closeButton' onClick={() => {inputPlayerNumber.clear(); inputRoundTime.clear(); inputRoundCount.clear(); setModalCreateActive(false)}}>+</button>
                </form>
            </Modal>
        </div>)

}

Create.propTypes = {
    name: PropTypes.string.isRequired
}

export default Create
