import React, {useState} from 'react'
import PropTypes from 'prop-types'
import {useNavigate} from "react-router-dom";
import Modal from "../../../Modal/Modal";
import './Join.css'

function useInputValue(defaultValue = '') {
    const [value, setValue] = useState(defaultValue)

    return {
        bind: {
            value,
            onChange: event => setValue(event.target.value),
            placeholder: "код для подключения",
            fontSize: '100px',
            size: '23'
        },
        clear: () => setValue(''),
        value: () => value
    }
}

function Join(props) {
    const input = useInputValue()
    const navigate = useNavigate()

    const [modalJoinActive, setModalJoinActive] = React.useState(false)
    const [error, setError] = React.useState('')

    function submitCode(event) {
        event.preventDefault()

        let code = Number.parseInt(input.value().trim())
        if (!isNaN(code)) {
            navigate('/game', {replace: true, state: {name: props.name, numberOfPlayers: -1, code: code, token: props.token}})
        } else {
            setError('Код может состоять только из цифр!')
        }
        input.clear()
    }

    return (
        <div>
            <button className='joinButton' onClick={() => {setModalJoinActive(true); setError('')}}>JOIN GAME</button>
            <Modal active={modalJoinActive} setActive={setModalJoinActive}>
                <form className='joinButtonsGroup' style={{marginBottom: '1rem'}} onSubmit={submitCode}>
                    <div>
                        <p>Username: {props.name}</p>
                        <p style={{
                            float: 'right',
                            color: 'red',
                            fontSize: '12pt'
                        }}>{error}</p>
                    </div>
                    <input {...input.bind} /><br/>
                    <button className='jButton' type='submit'>join</button>
                    <button className='closeButton' onClick={() => {input.clear(); setModalJoinActive(false)}}>+</button>
                    
                </form>
            </Modal>
        </div>
    )
}

Join.propTypes = {
    name: PropTypes.string.isRequired
}

export default Join
