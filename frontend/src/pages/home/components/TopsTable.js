import React from 'react';
import PropTypes from "prop-types";
import Top from "./Top";
import "./TopsTable.css";

function TopsTable(props){
    return(
        <table border="1" width = "400" align="center">
            <thead>
            <tr className='topTitle'>
                <th>Name</th>
                <th>Score</th>
            </tr>
            </thead>
            <tbody>
            {props.tops ? props.tops.map((top, index) => {
                return (
                    <Top
                        username={top.username}
                        score={top.score}
                        key={index}
                    />
                )
            }) : <tr>Waiting for tops...</tr>}
            </tbody>
        </table>
    )
}

TopsTable.propTypes = {
    tops: PropTypes.array,
}

export default TopsTable;
