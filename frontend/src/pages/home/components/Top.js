import React from 'react';
import PropTypes from "prop-types";
import "./Top.css";


function Top(props) {
    return (
        <tr className='rows'>
            <td>{props.username}</td>
            <td>{props.score}</td>
        </tr>
    )
}

Top.propTypes = {
    username: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired
}

export default Top;
