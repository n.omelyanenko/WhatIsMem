import React from 'react'
import Name from './components/Name'
import Join from './components/Join'
import Create from './components/Create'
import Login from "./components/Login";
import Register from "./components/Register";
import AccountButton from "./components/AccountButton";
import TopsTable from "./components/TopsTable";
import Modal from "../../Modal/Modal";
import axios from "axios";
import './Home.css'

function Home() {
    const [name, setName] = React.useState('anonim')
    const [username, setUsername] = React.useState('')
    const [pwd, setPwd] = React.useState('')
    const [modal, setModal] = React.useState(false)
    const [token, setToken] = React.useState('-1')
    const [top, setTop] = React.useState([])
    function getTop(){
       (async () =>{
    let response= await axios.get('https://whatmemareyou.tech/api/v1/leaderBoard');
    setTop(response.data.top);
       console.log(top)})()
    }

    function getLogButtons() {
        if (token !== '-1') return (
            <div className='cabinetButtons'>
                <button className='logout' onClick={() => setToken('-1')}>Logout</button>
                <AccountButton username={username} password={pwd} token={token}/>
            </div>
        )
        else
            return (
                <div className='cabinetButtons2'>
                    <Login setName={setName} setUsername={setUsername} setPwd={setPwd} setToken={setToken}/>
                    <Register/>
                </div>
            )
    }

    return (
        <div className="main">
            <h1>What Is Mem?</h1>
            {getLogButtons()}
            <br></br> <Name name={name} setName={setName}/>
            <br></br>
            <Join name={name} token={token}/>
            <Create name={name} token={token}/>
            <button className='topButton' onClick={() => {setModal(true); getTop()}}>TOP players</button>
            <Modal active={modal} setActive={setModal}>
                <h5>Leaders table</h5>
                <TopsTable tops={top} />
            </Modal>
        </div>
    )
}

export default Home;

