import React from 'react'
import PropTypes from 'prop-types'
import './WaitModal.css'

function WaitModal(props) {
    return (
        <div className={props.active ? 'waitModal active' : 'waitModal'}>
            <div className='waitModal_content'>
                {props.children}
                <div className="loader"></div>
            </div>
        </div>
    )
}

WaitModal.propTypes = {
    active: PropTypes.bool.isRequired,
    setActive: PropTypes.func.isRequired
}

export default WaitModal
























