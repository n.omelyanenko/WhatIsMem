import React from 'react';
import PropTypes from "prop-types";

function Player(props) {
    let points = 0
    props.votes.map((vote) => {
        if (vote.userID === props.id){
            points = vote.totalVotes
        }
        return 0
    })
    return (
        <li>
            Player name: {props.player}, points: {points}
        </li>
    )
}

Player.propTypes = {
    id: PropTypes.number.isRequired,
    player: PropTypes.string.isRequired
}

export default Player;
