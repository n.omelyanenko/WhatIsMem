import React from 'react';
import PropTypes from "prop-types";


function Card(props) {
    return (
    <img src= {props.url} alt="mem" width="200" border="20"  id={String(props.id)} className='img'/>
    )
}

Card.propTypes = {
    id: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired
}

export default Card;
