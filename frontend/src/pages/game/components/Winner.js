import React from 'react'
import PropTypes from 'prop-types'
import "./Winner.css"

function Winner(props) {
    return (
        <h2 className="winnerTitle">{props.name} выигрывает игру, набрав очков: {props.points}</h2>
    )
}

Winner.propTypes = {
    name: PropTypes.string.isRequired,
    points: PropTypes.number.isRequired
}

export default Winner;
