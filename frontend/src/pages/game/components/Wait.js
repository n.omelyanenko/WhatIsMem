import React from 'react';
import PropTypes from "prop-types";
import "./Wait.css"

function Wait(props) {

    return (
        <div className='waitingWindow'>
            <p className='playerName'>Username: {props.username}</p>
            <p className='code'>Game code: {props.code}</p>
            <p className='code'>Rounds number: {props.rounds}</p>
            <p className='code'>Round time: {props.time} sec</p>
            <p className='connectedPlayers'>Connected players: {props.counter}/{props.number}</p>
        </div>
    )
}

Wait.propTypes = {
    username: PropTypes.string.isRequired,
    code: PropTypes.number.isRequired,
    counter: PropTypes.number,
    number: PropTypes.number,
    rounds: PropTypes.number,
    time: PropTypes.number
}

export default Wait;

