import React from 'react';
import PropTypes from "prop-types";
import Card from "./Card";
import './Cards.css';

let sendID;
let chosenCard;
function ChangeCard(event) {

    if (event.target.childElementCount === 0 && (chosenCard === -1 || event.target.id !== chosenCard)){
        let ID;
        ID=event.target.id;
        document.querySelectorAll('.cardsRow img').forEach(item=>item.classList.remove('active'));
        event.target.classList.add('active');
        sendID(ID);
    }
}

function Click(){
    if(document.querySelector('.cardsRow')!=null){
        const imageOut =document.querySelector('.cardsRow');
        imageOut.addEventListener("click",ChangeCard)}
}

function Cards(props) {

    sendID=props.sendID;
    chosenCard=props.chosenCard;
    Click()
    return (
        <p className='cardsList'>
            <p className='stage'> {(chosenCard === -1) ? "Выберите карту" : "Проголосуйте"} </p>
            <div className='cardsRow'>
                {props.cards ? props.cards.map((card) => {
                    return (
                        <Card
                            id={card.id}
                            key={card.id}
                            url={card.url}
                        />
                        )
                }) : <p>Waiting for cards...</p>}
            </div>
        </p>

    )
}

Cards.propTypes = {
    cards: PropTypes.array,
    sendID: PropTypes.func.isRequired
}



export default Cards;
