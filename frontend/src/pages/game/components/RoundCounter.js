import React from 'react'
import PropTypes from 'prop-types'
import "./RoundCounter.css"

function RoundCounter(props) {
    return (
        <p className='raund'>Раунд {props.round}/{props.total}</p>
    )
}

RoundCounter.propTypes = {
    round: PropTypes.number.isRequired,
    total: PropTypes.number.isRequired
}

export default RoundCounter;
