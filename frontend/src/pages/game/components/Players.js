import React from 'react';
import PropTypes from "prop-types";
import Player from "./Player";
import './Players.css'

function Players(props) {
    return (
        <div className='playersList'>
            <ul className='players'>
                {props.players ? props.players.map((player) => {
                    return (
                        <Player
                            id={player.id}
                            player={player.name}
                            points={player.points}
                            key={player.id}
                            votes={props.votes}
                        />
                    )
                }) : <p>No connected players</p>}
            </ul>
        </div>
    )
}

Players.propTypes = {
    players: PropTypes.array
}

export default Players;
