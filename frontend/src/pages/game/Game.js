import React from 'react';
import {useLocation, useNavigate} from 'react-router-dom'
import SockJS from 'sockjs-client'
import {over} from "stompjs";
import WaitModal from "./waitModal/WaitModal";
import Wait from "./components/Wait";
import Players from "./components/Players";
import Cards from "./components/Cards";
import Winner from "./components/Winner";
import RoundCounter from "./components/RoundCounter";
import Modal from "../../Modal/Modal";
import "./game.css"

let stompClient
let state1

let number = -1
let gameCode = -1
let playerCounter = 0
let totalRounds = 5
let roundTime = 5000
let update
let modal
let winnerModal

let playersF
let situationF
let cardsO
let chosenCardsO
let votesF
let flag = "choose"
let winnerF
let roundO

let chosenCard
let voteCard

const connect = () => {
    let Sock = new SockJS('https://whatmemareyou.tech/api/v1/ws')
    stompClient = over(Sock)
    console.log(state1.state.token)
    if (state1.state.token !== '-1') {
        stompClient.connect({token: state1.state.token}, onConnected, onError)
    } else {
        stompClient.connect({}, onConnected, onError)
    }
}

const onError = (err) => {
    console.log(err)
}

const onConnected = () => {

    if (state1.state.code === -1) {
        onCreate()
    } else {
        onJoin()
    }
}

const onCreate = () => {
    let chatMessage = {
        type: "CREATE",
        numberOfPlayer: state1.state.numberOfPlayers,
        roundTime: state1.state.roundTimeSec,
        roundCount: state1.state.numberOfRounds
    }
    stompClient.subscribe('/user/queue/create', onCreateMessageReceived)
    stompClient.send("/app/create", {}, JSON.stringify(chatMessage))
}

const onCreateMessageReceived = (payload) => {

    let payloadData = JSON.parse(payload.body)
    console.log(payloadData)
    let chatMessage = {
        type: "CONNECT",
        playerName: state1.state.name
    };
    gameCode = payloadData.roomID
    number = payloadData.numberOfPlayers

    stompClient.subscribe('/topic/room/' + gameCode.toString(), onWaitMessageReceived)
    stompClient.send("/app/room/" + gameCode.toString(), {}, JSON.stringify(chatMessage))
}

const onJoin = () => {
    let chatMessage = {
        type: "CONNECT",
        playerName: state1.state.name
    }
    gameCode = state1.state.code

    stompClient.subscribe('/topic/room/' + gameCode.toString(), onWaitMessageReceived)
    stompClient.send("/app/room/" + gameCode.toString(), {}, JSON.stringify(chatMessage))
}

const onWaitMessageReceived = (payload) => {
    let payloadData = JSON.parse(payload.body)
    console.log(payloadData)
    if (payloadData.type === "WAIT") {
        playerCounter = payloadData.current
        number = payloadData.max
    } else if (payloadData.type === "GAME_START") {
        console.log('Game started')
        playersF(payloadData.players)
        modal.setWaitModal(false)
        stompClient.subscribe('/topic/room/' + gameCode + '/game', onGameMessage)
        stompClient.subscribe('/user/queue/game', onCardsMessage)
    }
    roundTime = payloadData.roundTime
    totalRounds = payloadData.roundCount
    update()
}

const onGameMessage = (payload) => {
    let payloadData = JSON.parse(payload.body)
    console.log(payloadData)
    if (payloadData.type === "SITUATION") {
        situationF(payloadData.situationCard.text)
    } else if (payloadData.type === "SHOW_CHOSEN_CARDS") {
        flag = "vote"
        document.querySelectorAll('.cardsRow img').forEach(item => item.classList.remove('active'));
        chosenCardsO.setChosenCards(payloadData.cards)
        console.log('AAAA')
        setTimeout(() => {
            let chatMessage = {
                type: "VOTE",
                card: {id: voteCard}
            }
            if (chosenCard !== -1)
                stompClient.send('/app/room/' + gameCode.toString() + '/game', {}, JSON.stringify(chatMessage))
        }, roundTime * 1000 - 1200)
    } else if (payloadData.type === "SHOW_VOTE_RESULTS") {
        votesF(payloadData.votes)
        roundO.setRound(++roundO.round)
    } else if (payloadData.type === "END") {
        winnerF(payloadData.winner.name, payloadData.winner.points)
        winnerModal.setWinModal(true)
    }
}

const onCardsMessage = (payload) => {
    let payloadData = JSON.parse(payload.body)
    if (payloadData.type === "CARDS") {
        flag = "choose"
        document.querySelectorAll('.cardsRow img').forEach(item => item.classList.remove('active'));
        console.log(flag)
        console.log(typeof payloadData.cards)
        console.log(payloadData.cards)
        cardsO.setCards(payloadData.cards)

        setTimeout(() => {
            let chatMessage = {
                type: "CHOOSE",
                memeCard: {id: chosenCard}
            }
            if (chosenCard !== -1)
                stompClient.send('/app/room/' + gameCode.toString() + '/game', {}, JSON.stringify(chatMessage))
        }, roundTime * 1000 - 1200)
    }
}

function start() {
    console.log(state1)
    connect()
    playerCounter++
}

function Game() {
    const navigate = useNavigate()
    let [players, setPlayers] = React.useState([])
    let [situation, setSituation] = React.useState('')
    let [cards, setCards] = React.useState([])
    cardsO = {cards, setCards}
    let [votes, setVotes] = React.useState([])
    let [winnerName, setWinnerName] = React.useState('')
    let [winnerPoints, setWinnerPoints] = React.useState('')
    let [round, setRound] = React.useState(1)
    roundO = {round, setRound}
    let [chosenCards, setChosenCards] = React.useState([])
    chosenCardsO = {chosenCards, setChosenCards}
    playersF = (prop) => {
        setPlayers(prop)
    }
    situationF = (prop) => {
        setSituation(prop)
    }
    votesF = (prop) => {
        setVotes(prop)
    }
    winnerF = (name, points) => {
        setWinnerName(name)
        setWinnerPoints(points)
    }
    let [waitModal, setWaitModal] = React.useState(true)
    let [winModal, setWinModal] = React.useState(false)
    modal = {setWaitModal}
    winnerModal = {setWinModal}
    let [updateCounter, setUpdateCounter] = React.useState(0)
    update = () => {
        setUpdateCounter(++updateCounter)
    }
    state1 = useLocation()
    let [cardId, setCardId] = React.useState(-1)
    let [voteId, setVoteId] = React.useState(-1)
    if (playerCounter === 0) start()

    function sendID(id) {
        setCardId(cardId = id)
        chosenCard = cardId
        console.log("Chosen card: " + chosenCard)
    }

    function sendVoteID(id) {
        setVoteId(voteId = id)
        voteCard = voteId
        console.log("Voted card: " + voteCard)
    }

    return (
        <div>
            <WaitModal active={waitModal} setActive={setWaitModal}>
                <Wait username={state1.state.name} code={parseInt(gameCode)} counter={playerCounter} number={number} rounds={totalRounds} time={roundTime}/>
            </WaitModal>
            <RoundCounter round={round} total={totalRounds}/>
            <p className='situation'>Ситуация: {situation}</p>
            <p className='playersTitle'>Игроки: </p>
            <Players players={players} votes={votes}/>
            {flag === "choose" ? <Cards cards={cards} sendID={sendID} chosenCard={-1}/> :
                <Cards cards={chosenCards} sendID={sendVoteID} chosenCard={chosenCard}/>}
            <Modal active={winModal} setActive={() => true}>
                <Winner name={winnerName} points={winnerPoints} />
                <button className="againButton" onClick={() => navigate('/', {replace: true, state: {token: state1.state.token}})}>Play again?</button>
            </Modal>
        </div>

    )
}

export default Game;
