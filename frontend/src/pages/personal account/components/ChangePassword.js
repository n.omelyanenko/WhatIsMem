import React, {useState} from 'react'
import PropTypes from "prop-types";
import Modal from "../../../Modal/Modal"
import "./ChangePassword.css"
import axios from "axios";

function useInputValue(background = '', defaultValue = '') {
    const [value, setValue] = useState(defaultValue)
    return {
        bind: {
            value,
            onChange: event => setValue(event.target.value),
            placeholder: background,
            fontSize: '100px',
            size: '23'
        },
        clear: () => setValue(''),
        value: () => value
    }
}

function ChangePassword(props) {
    const inputPwd1 = useInputValue('New password')
    const inputPwd2 = useInputValue('Confirm new password')
    const [error, setError] = React.useState('')
    const [modalActive, setModalActive] = React.useState(false)
    axios.defaults.headers.common['Authorization'] = props.token

    function submitCode(event) {
        event.preventDefault()
        if (inputPwd1.value() === inputPwd2.value() && inputPwd1.value() !== '') {
            axios.post('https://whatmemareyou.tech/api/v1/change', {
                password: inputPwd1.value(),
                confirmPassword: inputPwd2.value()
            }/*, config*/)
                .then(function (response) {
                    console.log(response);
                    props.setPwd(inputPwd1.value());
                    //props.setToken(response.data.token)
                })
                .catch(function (error) {
                    console.log(error);
                    //props.setToken('-1')
                });
            setModalActive(false);
        } else {
            setError('Incorrect password repeat')
            inputPwd1.clear()
            inputPwd2.clear()
        }
    }

    return (
        <div>
            <p>Your password: {props.pwd}</p>
            <button className='changePassword' onClick={() => {
                setError('')
                setModalActive(true);
            }}>Change password
            </button>
            <Modal active={modalActive} setActive={setModalActive}>
                <form className='passwordForm' onSubmit={submitCode}>
                    <p>Change password</p>
                    <input {...inputPwd1.bind} />

                    <input {...inputPwd2.bind} />
                    <button className='lButton' type='submit'>Change</button>
                    <button className='closeButton1' id='closeClick' onClick={() => {
                        inputPwd1.clear();
                        inputPwd2.clear();
                        setModalActive(false)
                    }}>+
                    </button>
                    <p style={{
                        float: 'right',
                        color: 'red',
                        fontSize: '12pt',
                        position: 'absolute',
                        left: '43%'
                    }}>{error}</p>
                    <br></br>
                    <br></br>
                </form>
            </Modal>
        </div>)
}

ChangePassword.propTypes = {
    pwd: PropTypes.string.isRequired,
    setPwd: PropTypes.func.isRequired,
    token: PropTypes.string.isRequired,
}

export default ChangePassword
