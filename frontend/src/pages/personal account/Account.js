import React from 'react'
import ChangePassword from "./components/ChangePassword";
import {useLocation, useNavigate} from "react-router-dom";
import "./Account.css";
import axios from "axios";

function Account() {
    const state1 = useLocation()
    const navigate = useNavigate()
    const username = state1.state.username
    const [pwd, setPwd] = React.useState(state1.state.password)
    const [token, setToken] = React.useState(state1.state.token)
    const [score, setScore] = React.useState(-1)

    axios.defaults.headers.common['Authorization'] = token;
    function getScore(){
        (async () =>{
            let response= await axios.get('https://whatmemareyou.tech/api/v1/personalscore');
            setScore(response.data.score);
            console.log(score)})()
    }

    return (
        <div className="main">
            <h1>Hello, {username}</h1>
            {score === -1 ? getScore() : <p>Points number: {score}</p>}
            <ChangePassword token={state1.state.token} setToken={setToken} pwd={pwd} setPwd={setPwd}/>
            <button className='home' onClick={() => navigate('/', {replace: true, state: {user: username, token: token}})}>go HOME</button>
        </div>
    )
}

export default Account;
