import React from "react"
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Home from './pages/home/Home'
import Game from './pages/game/Game'
import Account from "./pages/personal account/Account";
import Layout from './Layout'



function App() {
    return (
            <Router>
                <Routes>
                    <Route path="/" element={<Layout/>}>
                        <Route index element={<Home/>}/>
                        <Route path="/game" element={<Game/>}/>
                        <Route path="/user" element={<Account/>}/>
                    </Route>
                </Routes>
            </Router>
    )
}

export default App


