package com.nsu.backend;

import messages.server.ServerConfirmMessage;
import messages.server.ServerErrorMessage;
import messages.server.ServerMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class GameTest {
    private GameController gameController;
    private List<Player> players;

    @Before
    public void setup() {
        Room room = new Room("1111", 2, 10, 10);
        players = new ArrayList<>();
        Player player1 = new Player(0, "Player1", "0", null);
        players.add(player1);
        Player player2 = new Player(1, "Player2", "1", null);
        players.add(player2);
        room.addPlayer(player1);
        room.addPlayer(player2);
        gameController = new GameController();
        gameController.setRoom(room);
        gameController.startGame();
        while (gameController.getGameStage() == GameStage.PAUSE) ;
    }

    @Test
    public void testPlayerHand() {
        Map<Player, Set<MemeCard>> hands = gameController.getPlayerHand();
        for (Player player : players) {
            assertEquals(hands.get(player).size(), gameController.getHandSize());
        }
    }

    @Test
    public void testRightCardChoose() {
        while (gameController.getGameStage() != GameStage.GAME) ;
        Map<Player, Set<MemeCard>> hands = gameController.getPlayerHand();
        for (Player player : players) {
            MemeCard memeCard = hands.get(player).stream().findAny().get();
            ServerMessage answer = gameController.setChosenCard(player, memeCard);
            assertTrue(answer instanceof ServerConfirmMessage);
        }
    }

    @Test
    public void testWrongCardChoose() {
        while (gameController.getGameStage() != GameStage.GAME) ;
        Map<Player, Set<MemeCard>> hands = gameController.getPlayerHand();
        List<MemeCard> memeCards = gameController.getMemeCards();
        for (Player player : players) {
            MemeCard memeCard = memeCards.stream().filter((x) -> !(hands.get(player).contains(x))).findAny().get();
            ServerMessage answer = gameController.setChosenCard(player, memeCard);
            assertTrue(answer instanceof ServerErrorMessage);
        }
    }

    @Test
    public void testRightVoteCardChoose() {
        while (gameController.getGameStage() == GameStage.VOTING) ;
        Map<Player, Set<MemeCard>> hands = gameController.getPlayerHand();
        MemeCard voted = null;
        for (Player player : players) {
            MemeCard memeCard = hands.get(player).stream().findAny().get();
            voted = memeCard;
            ServerMessage answer = gameController.setChosenCard(player, memeCard);
            assertTrue(answer instanceof ServerConfirmMessage);
        }
        while (gameController.getGameStage() == GameStage.GAME) ;
        for (Player player : players) {
            assertTrue(gameController.applyVote(player, voted) instanceof ServerConfirmMessage);
        }
        while (gameController.getGameStage() == GameStage.GAME) ;
    }

    @Test
    public void testVotedResult() {
        while (gameController.getGameStage() == GameStage.VOTING) ;
        Map<Player, Set<MemeCard>> hands = gameController.getPlayerHand();
        MemeCard voted = null;
        Player votedPlayer = null;
        int point = 0;
        for (Player player : players) {
            MemeCard memeCard = hands.get(player).stream().findAny().get();
            voted = memeCard;
            votedPlayer = player;
            point = votedPlayer.getPoints();
            ServerMessage answer = gameController.setChosenCard(player, memeCard);
            assertTrue(answer instanceof ServerConfirmMessage);
        }
        while (gameController.getGameStage() == GameStage.GAME) ;
        for (Player player : players) {
            gameController.applyVote(player, voted);
        }
        while (gameController.getGameStage() == GameStage.VOTING) ;
        //assertEquals(point + players.size(), votedPlayer.getPoints());
    }

}
