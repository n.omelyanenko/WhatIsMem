package com.nsu.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import messages.client.ClientConnectMessage;
import messages.client.ClientCreateMessage;
import messages.server.ServerCreateMessage;
import messages.server.ServerGameStartMessage;
import messages.server.ServerWaitMessage;
import org.checkerframework.checker.units.qual.C;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.converter.GsonMessageConverter;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebSocketTest {
    @Value("${local.server.port}")
    private int port;
    private String URL;
    private static final ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
    private final CompletableFuture<ServerCreateMessage> createCompletableFuture = new CompletableFuture<>();
    private final CompletableFuture<ServerWaitMessage> waitCompletableFuture = new CompletableFuture<>();
    private final CompletableFuture<ServerWaitMessage> secondWaitCompletableFuture = new CompletableFuture<>();

    @Before
    public void setup() {
        URL = "ws://localhost:" + port + "/api/v1/ws";
    }


    @Test
    public void testCreateGameEndpoint() throws URISyntaxException, InterruptedException, ExecutionException, TimeoutException, JsonProcessingException {
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        StompSession stompSession = stompClient.connect(URL, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);
        stompSession.subscribe("/user/queue/create", new CreateStompFrameHandler());
        ClientCreateMessage clientCreateMessage = new ClientCreateMessage(2, 10, 10);
        String json = ow.writeValueAsString(clientCreateMessage);
        stompSession.send("/app/create", json);
        ServerCreateMessage serverCreateMessage = createCompletableFuture.get(10, SECONDS);
        assertNotNull(serverCreateMessage);
    }

    @Test
    public void testConnectGameEndpoint() throws URISyntaxException, InterruptedException, ExecutionException, TimeoutException, JsonProcessingException {
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        final WebSocketHttpHeaders headers = new WebSocketHttpHeaders();
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        StompSession stompSession = stompClient.connect(URL, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);
        stompSession.subscribe("/user/queue/create", new CreateStompFrameHandler());
        ClientCreateMessage clientCreateMessage = new ClientCreateMessage(2, 10, 10);
        String json = ow.writeValueAsString(clientCreateMessage);
        stompSession.send("/app/create", json);
        ServerCreateMessage serverCreateMessage = createCompletableFuture.get(10, SECONDS);
        ClientConnectMessage clientConnectMessage = new ClientConnectMessage("player");
        json = ow.writeValueAsString(clientConnectMessage);
        stompSession.subscribe("/topic/room/" + serverCreateMessage.getRoomID(), new ConnectStompFrameHandler());
        stompSession.send("/app/room/" + serverCreateMessage.getRoomID(), json);
        ServerWaitMessage serverWaitMessage = waitCompletableFuture.get(10, SECONDS);
        assertNotNull(serverWaitMessage);
    }

    @Test
    public void testWaitPlayersNumberEndpoint() throws URISyntaxException, InterruptedException, ExecutionException, TimeoutException, JsonProcessingException {
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        final WebSocketHttpHeaders headers = new WebSocketHttpHeaders();
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        StompSession stompSession = stompClient.connect(URL, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);
        StompSession secondStompSession = stompClient.connect(URL, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);
        stompSession.subscribe("/user/queue/create", new CreateStompFrameHandler());
        ClientCreateMessage clientCreateMessage = new ClientCreateMessage(3, 10, 10);
        String json = ow.writeValueAsString(clientCreateMessage);
        stompSession.send("/app/create", json);
        ServerCreateMessage serverCreateMessage = createCompletableFuture.get(10, SECONDS);
        ClientConnectMessage clientConnectMessage = new ClientConnectMessage("player");
        json = ow.writeValueAsString(clientConnectMessage);
        stompSession.subscribe("/topic/room/" + serverCreateMessage.getRoomID(), new ConnectStompFrameHandler());
        stompSession.send("/app/room/" + serverCreateMessage.getRoomID(), json);
        ServerWaitMessage serverWaitMessage = waitCompletableFuture.get(10, SECONDS);
        secondStompSession.subscribe("/topic/room/" + serverCreateMessage.getRoomID(), new SecondConnectStompFrameHandler());
        clientConnectMessage = new ClientConnectMessage("player2");
        json = ow.writeValueAsString(clientConnectMessage);
        secondStompSession.send("/app/room/" + serverCreateMessage.getRoomID(), json);
        serverWaitMessage = secondWaitCompletableFuture.get(10, SECONDS);
        assertEquals(2, serverWaitMessage.getCurrent());
    }

    private List<Transport> createTransportClient() {
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        return transports;
    }

    private class CreateStompFrameHandler implements StompFrameHandler {
        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return ServerCreateMessage.class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            createCompletableFuture.complete((ServerCreateMessage) o);
        }
    }

    private class ConnectStompFrameHandler implements StompFrameHandler {
        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return ServerWaitMessage.class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            waitCompletableFuture.complete((ServerWaitMessage) o);
        }
    }

    private class SecondConnectStompFrameHandler implements StompFrameHandler {
        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return ServerWaitMessage.class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            secondWaitCompletableFuture.complete((ServerWaitMessage) o);
        }
    }

}
