package com.nsu.backend;

import java.util.concurrent.*;

public class GameExecutor {
    private GameExecutor() {
    }

    private static final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);

    public static void executeWithDelay(Runnable r, long delay, TimeUnit timeUnit) {
        executorService.schedule(r, delay, timeUnit);
    }
}
