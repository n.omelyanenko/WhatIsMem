package com.nsu.backend.security.service;

import com.nsu.backend.security.config.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class WebSocketAuthService {

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    public UsernamePasswordAuthenticationToken authenticate(String jwtToken) {
        String username = null;
        username = jwtTokenUtil.getUsernameFromToken(jwtToken);

        //Once we get the token validate it.
        if (username != null) {
            UserDetails userDetails = this.jwtUserDetailsService.loadUserByUsername(username);
            if (Boolean.TRUE.equals(jwtTokenUtil.validateToken(jwtToken, userDetails))) {
                return new UsernamePasswordAuthenticationToken(
                        userDetails, null, Collections.singleton((GrantedAuthority) () -> "USER"));
            }
        }
        throw new BadCredentialsException("Bad credentials");

    }


}

