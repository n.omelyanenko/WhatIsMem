package com.nsu.backend.security.controller;

import java.util.Collections;
import java.util.Objects;

import com.nsu.backend.security.model.*;
import com.nsu.backend.security.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.nsu.backend.security.config.JwtTokenUtil;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService jwtInMemoryUserDetailsService;

    @PostMapping(value = "/api/v1/change")
    public ResponseEntity<JwtResponse> changePassword(@RequestHeader("Authorization") String token,
                                            @RequestBody ChangePasswordRequest request) {
        var username = jwtTokenUtil.getUsernameFromToken(token);
        if (request.getPassword().equals(request.getConfirmPassword())) {
            var res = jwtInMemoryUserDetailsService.changePassword(username, request.getPassword());
            if (res) {
                return authenticateUser(new JwtRequest(username, request.getPassword()));
            }
            return ResponseEntity.status(400).body(new JwtResponse());
        }
        return ResponseEntity.status(400).body(new JwtResponse());
    }

    @GetMapping(value = "/api/v1/personalscore")
    public ResponseEntity<ScoreResponse> personalScore(@RequestHeader("Authorization") String token) {
        var username = jwtTokenUtil.getUsernameFromToken(token);
        var score = jwtInMemoryUserDetailsService.getScoreOf(username);
        if (score == null) {
            return ResponseEntity.status(400).body(new ScoreResponse());
        }
        return ResponseEntity.ok(new ScoreResponse(score));
    }

    @PostMapping(value = "/api/v1/register")
    public ResponseEntity<String> registerUser(@RequestBody RegisterRequest registerRequest) {
        try {
            jwtInMemoryUserDetailsService.loadUserByUsername(registerRequest.getUsername());
            return ResponseEntity.status(409).body("User with this username already exists.");
        } catch (UsernameNotFoundException e) {
            if (jwtInMemoryUserDetailsService.registerUser(registerRequest)) {
                return ResponseEntity.ok("Success");
            } else {
                return ResponseEntity.status(400).body("Invalid username/password");
            }
        }
    }

    @PostMapping(value = "/api/v1/authenticate")
    public ResponseEntity<JwtResponse> authenticateUser(@RequestBody JwtRequest authenticationRequest) {
        authenticate(authenticationRequest.getUsername(),authenticationRequest.getPwd());
        try {
            final UserDetails userDetails = jwtInMemoryUserDetailsService
                    .loadUserByUsername(authenticationRequest.getUsername());

            final String token = jwtTokenUtil.generateToken(userDetails);
            return ResponseEntity.ok(new JwtResponse(token));
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.status(401).body(new JwtResponse());
        }
    }

    public UsernamePasswordAuthenticationToken authenticate(String username, String pwd) {
        var token = new UsernamePasswordAuthenticationToken(username, pwd, Collections.singleton((GrantedAuthority) () -> "USER"));
        authenticationManager.authenticate(token);
        return token;
    }
}
