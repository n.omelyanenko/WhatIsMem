package com.nsu.backend.security.service;

import com.nsu.backend.security.JwtUserDetails;
import com.nsu.backend.security.User;
import com.nsu.backend.security.UserRepo;
import com.nsu.backend.security.model.RegisterRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	@Autowired
	UserRepo userRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		var user = userRepo.findByUsername(username);
		user.orElseThrow(() -> new UsernameNotFoundException("User with username " + username + " not found"));
		return new JwtUserDetails(user);
	}

	public Long addScore(String username, int score) throws UsernameNotFoundException {
		var u = userRepo.findByUsername(username);
		u.orElseThrow(() -> new UsernameNotFoundException("Cannot add score to user. Username not found."));
		var newScore = u.get().getScore() + score;
		u.get().setScore(newScore);
		userRepo.save(u.get());
		return newScore;
	}

	public boolean registerUser(RegisterRequest registerRequest) {
		if (!isValid(registerRequest)) {
			return false;
		}
		var u = new User();
		u.setScore(0L);
		u.setUsername(registerRequest.getUsername());
		u.setPassword(new BCryptPasswordEncoder().encode(registerRequest.getPassword()));
		u.setActive(true);
		u.setRoles("USER");
		userRepo.save(u);
		return true;
	}

	public Long getScoreOf(String username) {
		var opUser = userRepo.findByUsername(username);
		if (opUser.isPresent()) {
			var user = opUser.get();
			return user.getScore();
		}
		return null;
	}

	public boolean changePassword(String username, String pwd) {
		var opUser = userRepo.findByUsername(username);
		if (opUser.isPresent()) {
			var user = opUser.get();
			user.setPassword(new BCryptPasswordEncoder().encode(pwd));
			userRepo.save(user);
			return true;
		}
		return false;
	}

	public boolean isValid(RegisterRequest registerRequest) {
		if (registerRequest.getUsername().length() < 3) {
			return false;
		}
		if (registerRequest.getPassword().length() < 3) {
			return false;
		}
		if (!registerRequest.getPassword().equals(registerRequest.getConfirmPassword())) {
			return false;
		}
		return true;
	}

}