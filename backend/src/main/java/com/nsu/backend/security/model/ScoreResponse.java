package com.nsu.backend.security.model;

public class ScoreResponse {
    private Long score;

    public ScoreResponse() {
    }

    public ScoreResponse(Long score) {
        this.score = score;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }
}
