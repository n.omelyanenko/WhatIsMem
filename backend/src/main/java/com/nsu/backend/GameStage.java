package com.nsu.backend;

public enum GameStage {
    PAUSE, GAME, VOTING
}
