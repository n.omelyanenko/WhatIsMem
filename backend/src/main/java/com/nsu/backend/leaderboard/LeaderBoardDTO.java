package com.nsu.backend.leaderboard;

import java.util.List;

public class LeaderBoardDTO {
    private Integer amount;
    private List<UserScore> top;

    public LeaderBoardDTO() {
    }

    public LeaderBoardDTO(Integer amount, List<UserScore> top) {
        this.amount = amount;
        this.top = top;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public List<UserScore> getTop() {
        return top;
    }

    public void setTop(List<UserScore> top) {
        this.top = top;
    }
}
