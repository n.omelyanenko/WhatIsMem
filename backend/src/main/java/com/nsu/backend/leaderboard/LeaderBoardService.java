package com.nsu.backend.leaderboard;

import com.nsu.backend.security.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Comparator;

@Service
public class LeaderBoardService {
    @Autowired
    UserRepo userRepo;
    private Integer amount = 10;
    private final ArrayList<UserScore> top = new ArrayList<>();

    public void checkCandidate(String username, Long score) {
        var isLeader = false;
        for (var u : top) {
            if (username.equals(u.getUsername())) {
                u.setScore(score);
                isLeader = true;
            }
        }
        if (!isLeader) {
            if (top.size() < amount) {
                top.add(new UserScore(username, score));
            } else {
                var lowestScore = top.get(amount - 1).getScore();
                if (lowestScore < score) {
                    top.get(amount - 1).setScore(score);
                    top.get(amount - 1).setUsername(username);
                }
            }
        }
        top.sort(Comparator.comparing(UserScore::getScore).reversed());
    }

    public LeaderBoardDTO getLeaderBoard() {
        return new LeaderBoardDTO(amount, top);
    }

    @PostConstruct
    public void initLeaderBoard() {
        var users = userRepo.findAll();
        if (users.size() < amount) {
            amount = users.size();
        }
        for (var u : users) {
            checkCandidate(u.getUsername(), u.getScore());
        }
    }
}
