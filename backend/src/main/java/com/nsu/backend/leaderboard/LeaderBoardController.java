package com.nsu.backend.leaderboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class LeaderBoardController {
    @Autowired
    LeaderBoardService leaderBoardService;

    @GetMapping(value = "/api/v1/leaderBoard")
    public LeaderBoardDTO leaderBoard() {
        return leaderBoardService.getLeaderBoard();
    }
}
