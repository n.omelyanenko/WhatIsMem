package com.nsu.backend;


import messages.server.ServerMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import java.util.Map;


@Controller
public class WebSocketController {
    @Autowired
    MessageProcess messageProcessor;

    @MessageMapping("/create")
    @SendToUser("/queue/create")
    public ServerMessage createRoom(@Payload String clientCreateMessage, SimpMessageHeaderAccessor headerAccessor) {
        return messageProcessor.process(clientCreateMessage, headerAccessor);
    }

    @MessageMapping("/room/{roomId}")
    @SendTo("/topic/room/{roomId}")
    public ServerMessage joinRoom(@Payload String message, @DestinationVariable String roomId, SimpMessageHeaderAccessor headerAccessor) {
        Map<String, Object> attributes;
        if ((attributes = headerAccessor.getSessionAttributes()) != null) {
            attributes.put("roomId", roomId);
        }
        return messageProcessor.process(message, headerAccessor);
    }

    @MessageMapping("/room/{roomId}/game")
    @SendToUser("/queue/game")
    public ServerMessage gameProcess(@Payload String message, @DestinationVariable String roomId, SimpMessageHeaderAccessor headerAccessor) {
        return messageProcessor.process(message, headerAccessor);
    }

    @MessageExceptionHandler
    @SendToUser("/queue/errors")
    public String handleException(Throwable exception) {
        return exception.getMessage();
    }

}
