package com.nsu.backend;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
@JsonIgnoreProperties(value = {"player"})
public class MemeCard {
    private int id;
    private String url;
    private Player player;
    @JsonCreator
    public MemeCard(@JsonProperty(value = "id", required = true) Integer id, @JsonProperty(value = "url") String url) {
        this.id = id;
        this.url = url;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MemeCard memeCard = (MemeCard) o;
        return id == memeCard.id && Objects.equals(url, memeCard.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, url);
    }
}
