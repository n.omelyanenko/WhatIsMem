package com.nsu.backend;

public class VoteResult {
    private int userID;
    private MemeCard memeCard;
    private int roundVotes;
    private int totalVotes;

    public VoteResult(int userID, int roundVotes, MemeCard memeCard, int totalVotes) {
        this.userID = userID;
        this.roundVotes = roundVotes;
        this.memeCard = memeCard;
        this.totalVotes = totalVotes;
    }

    public int getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(int totalVotes) {
        this.totalVotes = totalVotes;
    }

    public MemeCard getMemeCard() {
        return memeCard;
    }

    public void setMemeCard(MemeCard memeCard) {
        this.memeCard = memeCard;
    }

    public VoteResult() {
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getRoundVotes() {
        return roundVotes;
    }

    public void setRoundVotes(int roundVotes) {
        this.roundVotes = roundVotes;
    }
}
