package com.nsu.backend;

import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class RoomService {
    private static final int MAX = 9999;
    private static final int MIN = 1000;
    private final ConcurrentHashMap<String, Room> rooms = new ConcurrentHashMap<>();
    private final Set<String> roomsSet = ConcurrentHashMap.newKeySet();
    private final Random random = new Random();

    public Room createRoom(int players,int roundTime,int roundCount) {
        String id;
        do {
            id = String.format("%04d", random.nextInt((MAX - MIN)) + MIN);
        } while (!roomsSet.add(id));
        Room room = new Room(id, players,roundTime,roundCount);
        rooms.put(id, room);
        return room;
    }

    public void freeRoom(Room room) {
        rooms.remove(room.getID());
        roomsSet.add(room.getID());
    }

    public Room getRoom(String roomId) {
        return rooms.get(roomId);
    }
}
