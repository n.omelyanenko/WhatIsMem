package com.nsu.backend;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class InitExecutor {
    private InitExecutor(){
    }
    private static final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    public static void execute(Runnable r) {
        executor.schedule(r,1, TimeUnit.SECONDS);
    }
}
