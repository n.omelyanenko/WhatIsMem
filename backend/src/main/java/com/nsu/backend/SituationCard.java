package com.nsu.backend;

public class SituationCard {
    private String text;

    public SituationCard() {
    }

    public SituationCard(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
