package com.nsu.backend;

import messages.server.ServerWaitMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.Map;

@Component
public class WebSocketEventListener {
    @Autowired
    private MessageSender messageSender;

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
        Map<String, Object> attributes;
        if ((attributes = headerAccessor.getSessionAttributes()) != null) {
            Room room = (Room) attributes.get("room");
            if (room != null) {
                String roomId = room.getID();
                Player player = (Player) attributes.get("player");
                if (player != null) {
                    room.removePlayer(player);
                    ServerWaitMessage message = new ServerWaitMessage(room.getPlayersCount(), room.getMaxPlayers(), Action.PLAYERDISCONNECTED, room.getRoundTime(), room.getRoundCount());
                    messageSender.sendToRoom(roomId, message);
                }
            }
        }
    }
}