package com.nsu.backend;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.security.Principal;
import java.util.concurrent.atomic.AtomicBoolean;

@JsonIgnoreProperties(value = {"sessionId", "isVoted", "principal"})
public class Player {
    private int id;
    private String name;
    private String sessionId;
    private int points;
    private Principal principal;
    private final AtomicBoolean isVoted = new AtomicBoolean();

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Player(int id, String name, String sessionId, Principal principal) {
        this.id = id;
        this.name = name;
        this.sessionId = sessionId;
        this.principal = principal;
    }

    public AtomicBoolean getIsVoted() {
        return isVoted;
    }

    public void setIsVoted(boolean isVoted) {
        this.isVoted.set(isVoted);
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Player() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Principal getPrincipal() {
        return principal;
    }

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }
}
