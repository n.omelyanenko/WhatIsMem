package com.nsu.backend;

import messages.client.*;
import messages.server.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MessageProcess {
    @Autowired
    ObjectMapper mapper;
    @Autowired
    RoomService roomService;
    private final static String WRONG_MESSAGE_FORMAT = "WRONG MESSAGE FORMAT: ";

    public ServerMessage process(String message, SimpMessageHeaderAccessor headerAccessor) {
        ClientMessage clientMessage;
        try {
            clientMessage = mapper.readValue(message, ClientMessage.class);
        } catch (JsonProcessingException e) {
            return new ServerErrorMessage(WRONG_MESSAGE_FORMAT + e.getMessage());
        }
        Player player = null;
        Room room;
        GameController gameController = null;
        Map<String, Object> attributes;
        if (clientMessage.getType() != ClientMessageType.CREATE && clientMessage.getType() != ClientMessageType.CONNECT) {
            if ((attributes = headerAccessor.getSessionAttributes()) != null) {
                String roomId = (String) attributes.get("roomId");
                room = roomService.getRoom(roomId);
                player = (Player) attributes.get("player");
                gameController = room.getGameController();
                if (player == null) {
                    return new ServerErrorMessage("CONNECT TO GAME FIRST");
                }
                if (gameController == null) {
                    return new ServerErrorMessage("WAIT GAME START");
                }
            } else {
                return new ServerErrorMessage("INTERNAL ERROR");
            }
        }
        switch (clientMessage.getType()) {
            case CREATE:
                return processCreate(message);
            case CONNECT:
                return processConnect(message, headerAccessor);
            case CHOOSE:
                return processChoose(message, player, gameController);
            case VOTE:
                return processVote(message, player, gameController);
            default:
                return new ServerErrorMessage("UNKNOWN MESSAGE TYPE");
        }
    }

    private ServerMessage processCreate(String message) {
        ClientCreateMessage clientCreateMessage;
        try {
            clientCreateMessage = mapper.readValue(message, ClientCreateMessage.class);
        } catch (JsonProcessingException e) {
            return new ServerErrorMessage(WRONG_MESSAGE_FORMAT + e.getMessage());
        }
        return new ServerCreateMessage(roomService.createRoom(clientCreateMessage.getNumberOfPlayer(), clientCreateMessage.getRoundTime(), clientCreateMessage.getRoundCount()).getID());
    }

    private ServerMessage processConnect(String message, SimpMessageHeaderAccessor headerAccessor) {
        ClientConnectMessage clientConnectMessage;
        try {
            clientConnectMessage = mapper.readValue(message, ClientConnectMessage.class);
        } catch (JsonProcessingException e) {
            return new ServerErrorMessage(WRONG_MESSAGE_FORMAT + e.getMessage());
        }
        Map<String, Object> attributes;
        if ((attributes = headerAccessor.getSessionAttributes()) != null) {
            String roomId = (String) attributes.get("roomId");
            Room room = roomService.getRoom(roomId);
            if (room != null) {
                Player player = new Player(room.getNextPlayerId(), clientConnectMessage.getPlayerName(), headerAccessor.getSessionId(), headerAccessor.getUser());
                attributes.put("playerName", clientConnectMessage.getPlayerName());
                attributes.put("room", room);
                attributes.put("player", player);
                room.addPlayer(player);
                if (room.getPlayersCount() == room.getMaxPlayers()) {
                    room.startGame();
                    return new ServerGameStartMessage(room.getPlayers(), room.getRoundTime(), room.getRoundCount());
                }
                return new ServerWaitMessage(room.getPlayersCount(), room.getMaxPlayers(), Action.PLAYERJOIN, room.getRoundTime(), room.getRoundCount());
            } else {
                return new ServerErrorMessage("WRONG ROOM CODE");
            }
        }
        return new ServerErrorMessage("INTERNAL ERROR");
    }

    private ServerMessage processChoose(String message, Player player, GameController gameController) {
        ClientChooseMessage clientChooseMessage;
        try {
            clientChooseMessage = mapper.readValue(message, ClientChooseMessage.class);
        } catch (JsonProcessingException e) {
            return new ServerErrorMessage(WRONG_MESSAGE_FORMAT + e.getMessage());
        }
        MemeCard memeCard = clientChooseMessage.getMemeCard();
        return gameController.setChosenCard(player, memeCard);

    }

    private ServerMessage processVote(String message, Player player, GameController gameController) {
        try {
            ClientVoteMessage clientVoteMessage = mapper.readValue(message, ClientVoteMessage.class);
            return gameController.applyVote(player, clientVoteMessage.getCard());
        } catch (JsonProcessingException e) {
            return new ServerErrorMessage(WRONG_MESSAGE_FORMAT + e.getMessage());
        }
    }


}
