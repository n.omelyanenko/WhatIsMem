package com.nsu.backend;

import com.nsu.backend.leaderboard.LeaderBoardService;
import com.nsu.backend.security.service.JwtUserDetailsService;
import messages.server.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class GameController {
    private RoomService roomService;
    private final JwtUserDetailsService userDetailsService;
    private final LeaderBoardService leaderBoardService;

    private final MessageSender messageSender;
    private final AtomicInteger roundNumber = new AtomicInteger(0);
    private Room room;
    private final Map<Player, Set<MemeCard>> playerHand = new ConcurrentHashMap<>();
    private static final List<MemeCard> memeCards = new ArrayList<>();
    private static final List<SituationCard> situationCards = new ArrayList<>();
    private volatile GameStage gameStage = GameStage.PAUSE;
    private static final int PLAYER_HAND_SIZE = 5;
    private int roundTimeSeconds = 10;
    private int roundsNumber = 5;
    private final Map<MemeCard, Integer> chosenCards = new ConcurrentHashMap<>();

    public GameController() {
        roomService = SpringContextConfig.getBean(RoomService.class);
        messageSender = SpringContextConfig.getBean(MessageSender.class);
        userDetailsService = SpringContextConfig.getBean(JwtUserDetailsService.class);
        leaderBoardService = SpringContextConfig.getBean(LeaderBoardService.class);
        initMemeCards();
        initSituationCards();
    }

    private void initMemeCards() {
        if (memeCards.isEmpty()) {
            synchronized (GameController.class) {
                if (memeCards.isEmpty()) {
                    InputStream is = getClass().getClassLoader().getResourceAsStream("memes.txt");
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
                        String line;
                        for (int i = 0; (line = br.readLine()) != null; ++i) {
                            memeCards.add(new MemeCard(i, line));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public GameStage getGameStage() {
        return gameStage;
    }

    public static List<MemeCard> getMemeCards() {
        return memeCards;
    }

    private void initSituationCards() {
        if (situationCards.isEmpty()) {
            synchronized (GameController.class) {
                if (situationCards.isEmpty()) {
                    InputStream is = getClass().getClassLoader().getResourceAsStream("situations.txt");
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
                        String line;
                        while ((line = br.readLine()) != null) {
                            situationCards.add(new SituationCard(line));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public int getHandSize() {
        return PLAYER_HAND_SIZE;
    }

    public Map<Player, Set<MemeCard>> getPlayerHand() {
        return playerHand;
    }


    public Map<MemeCard, Integer> getChosenCards() {
        return chosenCards;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void startGame() {
        if (room.getRoundTime() > 0 && room.getRoundTime() <= 60) {
            roundTimeSeconds = room.getRoundTime();
        }
        if (room.getRoundCount() > 0 && room.getRoundCount() <= 10) {
            roundsNumber = room.getRoundCount();
        }
        InitExecutor.execute(this::startRound);

    }

    private void sendPlayerCards() {
        var hands = initPlayerCards();
        int i = 0;
        for (Player player : room.getPlayers()) {
            Set<MemeCard> hand = hands.get(i);
            for (MemeCard memeCard : hand) {
                memeCard.setPlayer(player);
            }
            playerHand.put(player, hand);
            sendCards(player, hand);
            ++i;
        }
    }

    private void startRound() {
        if (roundNumber.get() != 0) {
            if (roundNumber.get() < roundsNumber) {
                sendVoteResults();
            } else {
                endGame();
                return;
            }
        }
        sendPlayerCards();
        chosenCards.clear();
        roundNumber.incrementAndGet();
        gameStage = GameStage.GAME;
        for (Player p : room.getPlayers()) {
            p.setIsVoted(false);
        }
        sendSituationCard(situationCards.get(ThreadLocalRandom.current().nextInt(0, situationCards.size())));
        GameExecutor.executeWithDelay(this::startVoting, roundTimeSeconds, TimeUnit.SECONDS);
    }

    private void startVoting() {
        gameStage = GameStage.VOTING;
        sendChosenCards();
        GameExecutor.executeWithDelay(this::startRound, roundTimeSeconds, TimeUnit.SECONDS);
    }

    public ServerMessage setChosenCard(Player player, MemeCard card) {
        if (gameStage != GameStage.GAME) {
            return new ServerErrorMessage("NOT GAME STAGE NOT");
        }
        Set<MemeCard> hand = playerHand.get(player);
        if (hand == null) {
            return new ServerErrorMessage("EMPTY CARDS");
        }
        MemeCard playerCard = memeCards.get(card.getId());
        if (hand.contains(playerCard)) {
            chosenCards.put(playerCard, 0);
            hand.remove(playerCard);
            return new ServerConfirmMessage("ACCEPT CHOOSE");
        }
        return new ServerErrorMessage("WRONG MEM CARD");
    }


    public ServerMessage applyVote(Player player, MemeCard card) {
        if (gameStage != GameStage.VOTING) {
            return new ServerErrorMessage("NOT VOTE STAGE NOT");
        }
        MemeCard memeCard = memeCards.get(card.getId());
        if (!chosenCards.containsKey(memeCard)) {
            return new ServerErrorMessage("WRONG MEME CARD");
        }
        AtomicBoolean isVoted = player.getIsVoted();
        if (isVoted.get()) {
            return new ServerErrorMessage("ALREADY VOTED");
        } else {
            if (isVoted.compareAndSet(false, true)) {
                chosenCards.compute(memeCard, (k, v) -> v + 1);
                return new ServerConfirmMessage("VOTE APPLIED");
            } else {
                return new ServerErrorMessage("ALREADY VOTED");
            }
        }
    }

    private void sendChosenCards() {
        ServerShowChosenCardsMessage serverShowChosenCardsMessage = new ServerShowChosenCardsMessage();
        serverShowChosenCardsMessage.setCards(chosenCards.keySet());
        messageSender.sendToGameRoom(room.getID(), serverShowChosenCardsMessage);
    }

    private void sendSituationCard(SituationCard card) {
        ServerSituationMessage serverSituationMessage = new ServerSituationMessage(card);
        messageSender.sendToGameRoom(room.getID(), serverSituationMessage);
    }

    private void sendCards(Player player, Set<MemeCard> playerHand) {
        ServerCardsMessage serverCardsMessage = new ServerCardsMessage(playerHand);
        messageSender.sendToPlayer(player, serverCardsMessage);
    }

    private void sendVoteResults() {
        Set<VoteResult> voteResults = new HashSet<>();
        Set<Player> players = new HashSet<>(room.getPlayers());
        for (Map.Entry<MemeCard, Integer> entry : chosenCards.entrySet()) {
            MemeCard card = entry.getKey();
            Player player = card.getPlayer();
            players.remove(player);
            int vote = entry.getValue();
            player.setPoints(player.getPoints() + vote);
            VoteResult voteResult = new VoteResult(player.getId(), vote, card, player.getPoints());
            voteResults.add(voteResult);
        }
        for (Player player : players) {
            VoteResult voteResult = new VoteResult(player.getId(), 0, null, player.getPoints());
            voteResults.add(voteResult);
        }
        ServerShowVoteResultsMessage voteResultsMessage = new ServerShowVoteResultsMessage(voteResults);
        messageSender.sendToGameRoom(room.getID(), voteResultsMessage);

    }

    private void endGame() {
        for (Map.Entry<MemeCard, Integer> entry : chosenCards.entrySet()) {
            MemeCard card = entry.getKey();
            Player player = card.getPlayer();
            int vote = entry.getValue();
            player.setPoints(player.getPoints() + vote);
        }
        Set<VoteResult> result = new HashSet<>();
        Player winner = null;
        int maxVotes = -1;
        for (Player player : room.getPlayers()) {
            VoteResult voteResult = new VoteResult(player.getId(), 0, null, player.getPoints());
            if (maxVotes < player.getPoints()) {
                winner = player;
                maxVotes = player.getPoints();
            }
            result.add(voteResult);

            if (player.getPrincipal() != null) {
                var name = player.getPrincipal().getName();
                var score = userDetailsService.addScore(name, player.getPoints());
                leaderBoardService.checkCandidate(name, score);
            }

        }
        ServerGameEndMessage endMessage = new ServerGameEndMessage(result, winner);
        messageSender.sendToGameRoom(room.getID(), endMessage);
        roomService.freeRoom(room);
        room = null;
        playerHand.clear();
        memeCards.clear();
        situationCards.clear();
        chosenCards.clear();
    }

    private ArrayList<Set<MemeCard>> initPlayerCards() {
        var cardIDs = new ArrayList<Integer>();
        for (int i = 0; i < memeCards.size(); ++i) {
            cardIDs.add(i);
        }
        Collections.shuffle(cardIDs);
        var hands = new ArrayList<Set<MemeCard>>();
        for (int i = 0; i < room.getMaxPlayers(); ++i) {
            Set<MemeCard> hand = ConcurrentHashMap.newKeySet();
            for (int j = 0; j < PLAYER_HAND_SIZE; ++j) {
                var cardID = cardIDs.get(i * PLAYER_HAND_SIZE + j);
                hand.add(memeCards.get(cardID));
            }
            hands.add(hand);
        }
        return hands;
    }
}
