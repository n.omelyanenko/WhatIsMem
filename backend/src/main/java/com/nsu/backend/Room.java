package com.nsu.backend;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Room {
    private String id;

    private int roundTime;
    private int roundCount;
    private int maxPlayers;
    private final Set<Player> players = ConcurrentHashMap.newKeySet();
    private final AtomicInteger playerId = new AtomicInteger(0);
    private final AtomicBoolean gameStart = new AtomicBoolean();
    private volatile GameController gameController;

    public Room(String id, int maxplayersCount, int roundTime, int roundCount) {
        this.id = id;
        this.maxPlayers = maxplayersCount;
        this.roundTime = roundTime;
        this.roundCount = roundCount;
    }

    public int getRoundTime() {
        return roundTime;
    }

    public void setRoundTime(int roundTime) {
        this.roundTime = roundTime;
    }

    public int getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(int roundCount) {
        this.roundCount = roundCount;
    }

    public GameController getGameController() {
        return gameController;
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxplayersCount) {
        this.maxPlayers = maxplayersCount;
    }

    public int getPlayersCount() {
        return players.size();
    }

    public int getNextPlayerId() {
        return playerId.getAndIncrement();
    }

    public void addPlayer(Player player) {
        players.add(player);
    }

    public void removePlayer(Player player) {
        players.remove(player);
    }

    public void startGame() {
        if (gameStart.get()) return;
        if (gameStart.compareAndSet(false, true)) {
            GameController gc = new GameController();
            gc.setRoom(this);
            gc.startGame();
            gameController = gc;
        }
    }
}