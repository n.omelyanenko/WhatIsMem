package com.nsu.backend;

import messages.server.ServerMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class MessageSender {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    public void sendToRoom(String roomId, ServerMessage message) {
        executorService.execute(() -> messagingTemplate.convertAndSend(String.format("/topic/room/%s", roomId), message));
    }

    public void sendToGameRoom(String roomId, ServerMessage message) {
        executorService.execute(() -> messagingTemplate.convertAndSend(String.format("/topic/room/%s/game", roomId), message));
    }

    public void sendToPlayer(Player player, ServerMessage message) {
        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
        headerAccessor.setSessionId(player.getSessionId());
        headerAccessor.setLeaveMutable(true);
        executorService.execute(() -> messagingTemplate.convertAndSendToUser(player.getSessionId(), "/queue/game", message, headerAccessor.getMessageHeaders()));
    }

}
