package messages.server;

import com.nsu.backend.VoteResult;

import java.util.Set;

public class ServerShowVoteResultsMessage extends ServerMessage {
    private Set<VoteResult> votes;

    public ServerShowVoteResultsMessage(Set<VoteResult> votes) {
        this();
        this.votes = votes;
    }

    public Set<VoteResult> getVotes() {
        return votes;
    }

    public void setVotes(Set<VoteResult> votes) {
        this.votes = votes;
    }

    public ServerShowVoteResultsMessage() {
        super(ServerMessageType.SHOW_VOTE_RESULTS);
    }
}
