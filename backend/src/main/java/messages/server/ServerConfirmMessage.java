package messages.server;

public class ServerConfirmMessage extends ServerMessage{
    private String message;

    public ServerConfirmMessage() {
        super(ServerMessageType.CONFIRM);
    }

    public ServerConfirmMessage(String message) {
        this();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
