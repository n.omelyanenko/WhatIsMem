package messages.server;

import com.nsu.backend.Player;

import java.util.Set;

public class ServerGameStartMessage extends ServerMessage {
    private Set<Player> players;
    private int roundTime;
    private int roundCount;

    public ServerGameStartMessage(Set<Player> players, int roundTime, int roundCount) {
        this();
        this.players = players;
        this.roundTime = roundTime;
        this.roundCount = roundCount;
    }

    public int getRoundTime() {
        return roundTime;
    }

    public void setRoundTime(int roundTime) {
        this.roundTime = roundTime;
    }

    public int getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(int roundCount) {
        this.roundCount = roundCount;
    }

    public ServerGameStartMessage() {
        super(ServerMessageType.GAME_START);
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }
}
