package messages.server;

import com.nsu.backend.Player;
import com.nsu.backend.VoteResult;

import java.util.Set;

public class ServerGameEndMessage extends ServerMessage{
    private Set<VoteResult> votes;
    private Player winner;
    public ServerGameEndMessage() {
        super(ServerMessageType.END);
    }

    public ServerGameEndMessage(Set<VoteResult> votes, Player winner) {
        this();
        this.votes = votes;
        this.winner = winner;
    }

    public Set<VoteResult> getVotes() {
        return votes;
    }

    public void setVotes(Set<VoteResult> votes) {
        this.votes = votes;
    }

    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }
}
