package messages.server;

import com.nsu.backend.MemeCard;

import java.util.Set;

public class ServerShowChosenCardsMessage extends ServerMessage {
    private Set<MemeCard> cards;

    public ServerShowChosenCardsMessage() {
        super(ServerMessageType.SHOW_CHOSEN_CARDS);
    }

    public Set<MemeCard> getCards() {
        return cards;
    }

    public void setCards(Set<MemeCard> cards) {
        this.cards = cards;
    }
}
