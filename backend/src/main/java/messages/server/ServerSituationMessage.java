package messages.server;

import com.nsu.backend.SituationCard;

public class ServerSituationMessage extends ServerMessage {
    private SituationCard situationCard;

    public ServerSituationMessage(SituationCard situationCard) {
        this();
        this.situationCard = situationCard;
    }

    public ServerSituationMessage() {
        super(ServerMessageType.SITUATION);
    }

    public SituationCard getSituationCard() {
        return situationCard;
    }

    public void setSituationCard(SituationCard situationCard) {
        this.situationCard = situationCard;
    }
}
