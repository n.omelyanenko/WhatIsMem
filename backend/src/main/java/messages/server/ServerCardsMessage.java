package messages.server;

import com.nsu.backend.MemeCard;

import java.util.Set;

public class ServerCardsMessage extends ServerMessage {
    private Set<MemeCard> cards;

    public ServerCardsMessage() {
        super(ServerMessageType.CARDS);
    }

    public ServerCardsMessage(Set<MemeCard> cards) {
        this();
        this.cards = cards;
    }

    public Set<MemeCard> getCards() {
        return cards;
    }

    public void setCards(Set<MemeCard> cards) {
        this.cards = cards;
    }
}
