package messages.server;

public class ServerErrorMessage extends ServerMessage {
    private String errorMessage;

    public ServerErrorMessage() {
        super(ServerMessageType.ERROR);
    }

    public ServerErrorMessage(String errorMessage) {
        this();
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
