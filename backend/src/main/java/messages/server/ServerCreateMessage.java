package messages.server;

public class ServerCreateMessage extends ServerMessage {
    private String roomID;

    public String getRoomID() {
        return roomID;
    }

    public void setRoomID(String roomID) {
        this.roomID = roomID;
    }

    public ServerCreateMessage() {
        super(ServerMessageType.CREATE);
    }

    public ServerCreateMessage(String roomID) {
        this();
        this.roomID = roomID;
    }

}
