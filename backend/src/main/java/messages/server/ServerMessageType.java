package messages.server;

public enum ServerMessageType {
    CREATE,
    GAME_START,
    WAIT,
    SITUATION,
    CARDS,
    SHOW_CHOSEN_CARDS,
    SHOW_VOTE_RESULTS,
    ERROR,
    CONFIRM,
    END
}
