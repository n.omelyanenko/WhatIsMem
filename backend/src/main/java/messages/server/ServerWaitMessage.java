package messages.server;

import com.nsu.backend.Action;

public class ServerWaitMessage extends ServerMessage {
    private int current;
    private int max;
    private Action action;
    private int roundTime;
    private int roundCount;

    public ServerWaitMessage(int current, int max, Action action, int roundTime, int roundCount) {
        this();
        this.current = current;
        this.action = action;
        this.max = max;
        this.roundTime = roundTime;
        this.roundCount = roundCount;
    }

    public int getRoundTime() {
        return roundTime;
    }

    public void setRoundTime(int roundTime) {
        this.roundTime = roundTime;
    }

    public int getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(int roundCount) {
        this.roundCount = roundCount;
    }

    public ServerWaitMessage() {
        super(ServerMessageType.WAIT);
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
}

