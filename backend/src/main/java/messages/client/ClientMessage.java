package messages.client;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientMessage {
    private ClientMessageType type;

    @JsonCreator
    public ClientMessage(@JsonProperty(value = "type", required = true) ClientMessageType type) {
        this.type = type;
    }

    public ClientMessage() {
    }

    public ClientMessageType getType() {
        return type;
    }

    public void setType(ClientMessageType type) {
        this.type = type;
    }

}
