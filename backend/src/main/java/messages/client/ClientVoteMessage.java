package messages.client;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nsu.backend.MemeCard;

public class ClientVoteMessage extends ClientMessage {
    private MemeCard card;
    @JsonCreator
    public ClientVoteMessage(@JsonProperty(value = "card",required = true) MemeCard card) {
        this();
        this.card = card;
    }

    public ClientVoteMessage() {
        super(ClientMessageType.VOTE);
    }

    public MemeCard getCard() {
        return card;
    }

    public void setCard(MemeCard card) {
        this.card = card;
    }
}
