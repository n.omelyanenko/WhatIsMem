package messages.client;

public enum ClientMessageType {
    CREATE,
    CONNECT,
    CHOOSE,
    VOTE
}
