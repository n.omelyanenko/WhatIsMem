package messages.client;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientConnectMessage extends ClientMessage {
    private String playerName;

    @JsonCreator
    public ClientConnectMessage(@JsonProperty(value = "playerName", required = true) String playerName) {
        this();
        this.playerName = playerName;
    }

    public ClientConnectMessage() {
        super(ClientMessageType.CONNECT);
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
