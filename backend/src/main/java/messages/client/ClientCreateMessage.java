package messages.client;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientCreateMessage extends ClientMessage {
    private int numberOfPlayer;
    private int roundTime;
    private int roundCount;

    public ClientCreateMessage() {
        super(ClientMessageType.CREATE);
    }

    @JsonCreator
    public ClientCreateMessage(@JsonProperty(value = "numberOfPlayer", required = true) int numberOfPlayers, @JsonProperty(value = "roundTime") int roundTime, @JsonProperty(value = "roundCount") int roundCount) {
        this();
        this.numberOfPlayer = numberOfPlayers;
        this.roundTime = roundTime;
        this.roundCount = roundCount;
    }

    public int getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(int roundCount) {
        this.roundCount = roundCount;
    }

    public int getRoundTime() {
        return roundTime;
    }

    public void setRoundTime(int roundTime) {
        this.roundTime = roundTime;
    }

    public int getNumberOfPlayer() {
        return numberOfPlayer;
    }

    public void setNumberOfPlayer(int numberOfPlayer) {
        this.numberOfPlayer = numberOfPlayer;
    }

}
