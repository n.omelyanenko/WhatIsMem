package messages.client;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nsu.backend.MemeCard;

public class ClientChooseMessage extends ClientMessage {
    private MemeCard memeCard;

    @JsonCreator
    public ClientChooseMessage(@JsonProperty(value = "memeCard", required = true) MemeCard card) {
        this();
        this.memeCard = card;
    }

    public ClientChooseMessage() {
        super(ClientMessageType.CHOOSE);
    }

    public MemeCard getMemeCard() {
        return memeCard;
    }

    public void setMemeCard(MemeCard memeCard) {
        this.memeCard = memeCard;
    }
}
